We work on design projects that allow us to fundamentally re-think how people interact with buildings and the cultural environment of modern India.
At SDAARCHITECT, we seek to understand each unique architectural design project in its environmental, cultural, and historical context. 

The work we do at SDAARCHITECT engages with the complexities of architectural program and the contextual challenges of a rapidly transforming urban India, and we strive to design proposals that promote a strong sense of place. 

Inspired by tradition and modernity alike, we use technology and traditional techniques to bring the distinctive qualities of well-crafted materials and details into every project. 

The result of this process is architectural design that restores, regenerates, and elevates our collective experience of the urban environment.
